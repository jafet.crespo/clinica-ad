import PatientController from "../src/controllers/patient.controller.js";
import PatientService from "../src/service/patient.service.js";
import PatientEntity from "../src/persistence/patient.entity.js"
import connect from "../src/models/db.js";
/*
describe('PatientService', () => {
let connection;
let patientService;

  beforeEach(async () => {
    connection = jasmine.creaSpyObj("connection", ["query"]);
    patientService = new PatientService(connection);
  });

  it('should create a new patient', async () => {

    const patient = await patientService.create("John", "Doe", "Headache");

    expect(connection.query).toHaveBeenCalledWith(
      `
      INSERT INTO patient (name, lastName, symptoms)
      VALUES (?, ?, ?)
      `,
      ["John", "Doe", "Headache"]
    );

    expect(patient).toEqual(new Patient(1, "John", "Doe", "Headache"));
    /*
    const connection = await connect();
    patientService = new PatientService(connection);
    const patient = new Patient(null, 'Julio', 'Kohlberg', 'Headache, fever');
    await patientService.create('Julio', 'Kohlberg', 'Headache, fever');
    const patients = await patientService.getAll();
    expect(patients.length).toBe(1);
    expect(patients[0].name).toBe(patient.name);
    expect(patients[0].lastName).toBe(patient.lastName);
    expect(patients[0].symptoms).toBe(patient.symptoms);
    
  });
});
*/
describe('PatientService', () => {
  let connection;
  let patientService;
  let patientController;

  beforeEach(async () => {
    connection = await connect();
    patientService = new PatientService(connection);
    patientController = new PatientController(patientService);
  });

  afterEach(() => {
    
  });

  it("should return all patients", async () => {
    const patients = await patientController.findAll();
    expect(Array.isArray(patients)).toBeTruthy();
  });
});  

