import express, { json, urlencoded } from 'express';
import cors from 'cors';
import connect from "./models/db.js";
import PatientService from "./service/patient.service.js";
import PatientController from "./controllers/patient.controller.js";
const app = express();

app.use(cors());

app.use(json());

app.use(urlencoded({extended: true,}));

app.post("/patient", async (req, res) => {
  const connection = await connect();
  const patientService = new PatientService(connection);
  const patientController = new PatientController(patientService);
  await patientController.create(req, res);
  connection.end();
});

app.get("/", async (req, res) => {
  const connection = await connect();
  const patientService = new PatientService(connection);
  const patientController = new PatientController(patientService);
  await patientController.findAll(req, res);
  connection.end();
});

app.get("/patients/:id", async (req, res) => {
  const connection = await connect();
  const patientService = new PatientService(connection);
  const patientController = new PatientController(patientService);
  await patientController.findById(req, res);
  connection.end();
});

app.put("/patients/:id", async (req, res) => {
  const connection = await connect();
  const patientService = new PatientService(connection);
  const patientController = new PatientController(patientService);
  await patientController.update(req, res);
  connection.end();
});

app.delete("/patients/:id", async (req, res) => {
  const connection = await connect();
  const patientService = new PatientService(connection);
  const patientController = new PatientController(patientService);
  await patientController.delete(req, res);
  connection.end();
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}.`);
});

