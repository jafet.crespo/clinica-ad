export default class Patient {
  constructor(id, name, lastName, symptoms) {
    this.id = id;
    this.name = name;
    this.lastName = lastName;
    this.symptoms = symptoms;
  }
}
