export default class PatientController {
  constructor(patientService) {
    this.patientService = patientService;
  }


  async create(req, res) {
    const patient = req.body;
    const id = await this.patientService.create(patient);
    res.send({ patient });
  }

  async findAll(req, res) {
    const patients = await this.patientService.findAll();
    res.send(patients);
  }

  async findById(req, res) {
    const { id } = req.params;
    const patient = await this.patientService.findById(id);
    res.send(patient);
  }


  async update(req, res) {
    const { id } = req.params;
    const patient = req.body;
    const affectedRows = await this.patientService.update(id, patient);
    res.send({ patient });
  }

  async delete(req, res) {
    const { id } = req.params;
    await this.patientService.delete(id);
    res.send({ message: "Patient deleted successfully" });
  }

}