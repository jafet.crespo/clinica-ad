import { createConnection } from "../../node_modules/mysql2/promise.js";
import { HOST, USER, PASSWORD, DB } from "../../config/db.config.js";


async function connect() {
  const connection = await createConnection({
    host: HOST,
    user: USER,
    password: PASSWORD,
    database: DB,
  });

  return connection;
}

export default connect;
