import Patient from "../persistence/patient.entity.js";

export class PatientService {
  constructor(connection) {
    this.connection = connection;
  }

  async create(patient) {
    const { name, lastName, symptoms } = patient;
    const [result] = await this.connection.query(
      'INSERT INTO patient (name, lastName, symptoms) VALUES (?, ?, ?)',
      [name, lastName, symptoms]
    );
    return result.insertId;
  }

  async findAll() {
    const [rows] = await this.connection.query("SELECT * FROM patient");
    return rows.map(
      (row) => new Patient(row.id, row.name, row.lastName, row.symptoms)
    );
  }

  async findById(id) {
    const [rows] = await this.connection.query('SELECT * FROM patient WHERE id = ?', [id]);
    return new Patient(rows[0].id, rows[0].name, rows[0].lastName, rows[0].symptoms);
  }

  async update(id, patient) {
    const { name, lastName, symptoms } = patient;
    const [result] = await this.connection.query(
      'UPDATE patient SET name = ?, lastName = ?, symptoms = ? WHERE id = ?',
      [name, lastName, symptoms, id]
    );
    return result.affectedRows;
  }

  async delete(id) {
    const [result] = await this.connection.query('DELETE FROM patient WHERE id = ?', [id]);
    return result.affectedRows;
  }
}

export default PatientService;
