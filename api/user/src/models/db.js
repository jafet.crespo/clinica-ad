const mysql = require("mysql2/promise");
const dbConfig = require("../../config/db.config.js");

async function connect() {
  const connection = await mysql.createConnection({
    host: dbConfig.HOST,
    user: dbConfig.USER,
    password: dbConfig.PASSWORD,
    database: dbConfig.DB,
  });

  return connection;
}

module.exports = connect;
