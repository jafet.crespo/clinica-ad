const User = require("../persistence/user.entity.js");

class UserService {
  constructor(connection) {
    this.connection = connection;
  }

  async findById(id) {
    const query = `
      SELECT *
      FROM user
      WHERE id = ?
    `;
    const [rows] = await this.connection.query(query, [id]);
    if (rows.length === 0) {
      return null;
    }
    const user = new User(
      rows[0].id,
      rows[0].userName,
      rows[0].password
    );
    return user;
  }
}

module.exports = UserService;
