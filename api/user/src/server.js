const express = require('express');
const cors = require('cors');
const connect = require("./models/db.js")
const UserService = require("./service/user.service.js");
const UserController = require("./controllers/user.controller.js");
const app = express();

app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended: true,}));

app.get("/user/:id", async (req, res) => {
  const connection = await connect();
  const userService = new UserService(connection);
  const userController = new UserController(userService);
  await userController.findById(req, res);
  connection.end();
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}.`);
});

