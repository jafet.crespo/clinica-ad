class UserController {
  constructor(userService) {
    this.userService = userService;
  }

  async findById(req, res) {
    const { id } = req.params;
    const user = await this.userService.findById(id);
    if (!user) {
      res.status(404).send({ message: "User not found" });
      return;
    }
    res.send(user);
  }  

}

module.exports = UserController;