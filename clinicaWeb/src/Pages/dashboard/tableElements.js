const d = document;
const $form = d.getElementById("crudForm");

const validatePatient = (patient) => {
  return (
    patient.hasOwnProperty("name") &&
    patient.hasOwnProperty("lastName") &&
    patient.hasOwnProperty("symptoms")
  );
};

export function createPatientRow(patient) {
  if (typeof patient !== "object" || patient === null) return false;
  if (!validatePatient(patient)) return false;

  let $nombre = d.createElement("td");
  $nombre.textContent = patient.name;
  let $apellido = d.createElement("td");
  $apellido.textContent = patient.lastName;
  let $sintomas = d.createElement("td");
  $sintomas.textContent = patient.symptoms;
  let $botones = d.createElement("td");
  $botones.insertAdjacentHTML(
    "afterbegin",
    `<button class="edit" data-id=${patient.id}>editar</button><button class="delete" data-id=${patient.id}>eliminar</button>`
  );

  const rowData = d.createElement("tr");
  rowData.classList.add("patient-row");
  rowData.appendChild($nombre);
  rowData.appendChild($apellido);
  rowData.appendChild($sintomas);
  rowData.appendChild($botones);

  return rowData;
}

export function replaceData(data) {
  const $newPatientsTable = d.createElement("tbody");
  $newPatientsTable.setAttribute("id", "patientsData");
  $newPatientsTable.appendChild(data);

  const $patientsTable = d.getElementById("patientsData");
  const parentTable = $patientsTable.parentNode;
  parentTable.replaceChild($newPatientsTable, $patientsTable);
}

export function updateForm(e) {
  const $tabla = e.target.parentNode.parentNode;
  const $filas = $tabla.querySelectorAll("td");
  $form.name.value = $filas[0].textContent;
  $form.lastName.value = $filas[1].textContent;
  $form.symptoms.value = $filas[2].textContent;
  $form.id.value = e.target.dataset.id;
}
