import {
  createPatient,
  deletePatient,
  getAll,
  updatePatient,
} from "./dashboardCRUD.js";
import { updateForm } from "./tableElements.js";

const d = document;
const $form = d.getElementById("crudForm");

function sortClick() {
  d.addEventListener("click", (e) => {
    if (e.target.matches(".sort-input")) {
      getAll(e.target.id);
    }
  });
}

function updateClick() {
  d.addEventListener("click", (e) => {
    if (e.target.matches(".edit")) {
      updateForm(e);
    }
  });
}

function deleteClick() {
  d.addEventListener("click", async (e) => {
    if (e.target.matches(".delete")) {
      deletePatient(e.target.dataset.id);
    }
  });
}

function submitClick() {
  d.addEventListener("submit", async (e) => {
    if (e.target === $form) {
      e.preventDefault();
      if (!e.target.id.value) {
        createPatient(e);
      } else {
        updatePatient(e);
      }
    }
  });
}

d.addEventListener("DOMContentLoaded", (e) => {
  getAll();
  sortClick();
  updateClick();
  submitClick();
  deleteClick();
});
