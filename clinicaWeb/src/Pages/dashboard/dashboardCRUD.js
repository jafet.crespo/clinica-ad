import ordenarBurbuja from "../../helpers/ordenarBurbuja.js";
import { createPatientRow, replaceData } from "./tableElements.js";

const url = "http://localhost:3000/pacientes";
const d = document;

export const getAll = async (ordenar = "name") => {
  const $fragment = d.createDocumentFragment();

  try {
    const resp = await fetch(url);
    if (!resp.ok) throw { status: resp.status, statusText: resp.statusText };
    const data = await resp.json();

    ordenarBurbuja(data, ordenar).forEach((patient) => {
      const rowData = createPatientRow(patient);
      $fragment.appendChild(rowData);
    });

    replaceData($fragment);
  } catch (err) {
    console.log(`Error: ${err.status} - ${err.statusText}`);
  }
};

export const createPatient = async (e) => {
  let symptoms = e.target.symptoms.value;

  try {
    let options = {
      method: "POST",
      headers: {
        "Content-type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({
        name: e.target.name.value,
        lastName: e.target.lastName.value,
        symptoms: symptoms,
      }),
    };

    let resp = await fetch(url, options);
    if (!resp.ok) throw { status: resp.status, statusText: resp.statusText };

    location.reload();
  } catch (error) {
    console.log(`Error: ${err.status} - ${err.statusText}`);
  }
};

export const deletePatient = async (id) => {
  let isDelete = confirm("¿estas seguro de eliminar el dato?");
  if (isDelete) {
    try {
      let options = {
        method: "DELETE",
        headers: {
          "Content-type": "application/json; charset=utf-8",
        },
      };

      let resp = await fetch(url + `/${id}`, options);
      if (!resp.ok) throw { status: resp.status, statusText: resp.statusText };

      location.reload();
    } catch (error) {
      console.log(`Error: ${err.status} - ${err.statusText}`);
    }
  }
};

export const updatePatient = async (e) => {
  let symptoms = e.target.symptoms.value;
  try {
    let options = {
      method: "PUT",
      headers: {
        "Content-type": "application/json; charset=utf-8",
      },
      body: JSON.stringify({
        name: e.target.name.value,
        lastName: e.target.lastName.value,
        symptoms: symptoms,
      }),
    };

    const resp = await fetch(url + `/${e.target.id.value}`, options);
    if (!resp.ok) throw { status: resp.status, statusText: resp.statusText };

    location.reload();
  } catch (error) {
    console.log(`Error: ${err.status} - ${err.statusText}`);
  }
};
