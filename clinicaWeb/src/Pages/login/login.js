import { validarUsuario } from "./validarLogin.js";

const d = document;
const $loginStatus = d.getElementById("loginText");

export default function login(form) {
  const $form = d.getElementById(form);
  d.addEventListener("submit", (e) => {
    e.preventDefault();
    if (e.target === $form) {
      const $username = d.getElementById("username").value;
      const $password = d.getElementById("password").value;
      if (validarUsuario($username, $password)) {
        $loginStatus.textContent = "";
        sessionStorage.setItem("username", $username);
        sessionStorage.setItem("password", $password);
        return (window.location.href = "../dashboard/dashboard.html");
      } else {
        $loginStatus.textContent = "credenciales incorrectos!!";
      }
    }
  });
}

document.addEventListener("DOMContentLoaded", (e) => {
  login("login");
});
