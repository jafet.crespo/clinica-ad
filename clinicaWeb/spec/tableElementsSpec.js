import {
  createPatientRow,
  replaceData,
} from "../src/Pages/dashboard/tableElements.js";

describe("tableElements function test", function () {
  let row, $fragment, $dataTable, $patientsTable;
  beforeEach(function () {
    row = document.createElement("tr");
    row.classList.add("patient-row");
    row.insertAdjacentHTML(
      "afterbegin",
      `<td>Name</td><td>LastName</td><td>Sintoma 1</td><td><button class="edit" data-id=${1}>editar</button><button class="delete" data-id=${1}>eliminar</button></td>`
    );

    $fragment = document.createDocumentFragment();
    $fragment.appendChild(row);

    $dataTable = document.createElement("table");
    $dataTable.id = "dataTable";

    $patientsTable = document.createElement("tbody");
    $patientsTable.id = "patientsData";

    $dataTable.insertAdjacentElement("afterbegin", $patientsTable);
  });

  it("Should return a row with pacient data", function () {
    let patient = {
      id: 1,
      name: "Name",
      lastName: "LastName",
      symptoms: "Sintoma 1",
    };
    let result = createPatientRow(patient);
    expect(result).toEqual(row);
  });

  it("checks for the existence of a edit button", function () {
    let patient = {
      id: 1,
      name: "Name",
      lastName: "LastName",
      symptoms: "Sintoma 1",
    };
    let result = createPatientRow(patient);
    expect(result.outerHTML).toContain("<button");
  });

  it("checks for the existence of a class edit", function () {
    let patient = {
      id: 1,
      name: "Name",
      lastName: "LastName",
      symptoms: "Sintoma 1",
    };
    const result = createPatientRow(patient).querySelector(".edit");
    expect(result).toHaveClass("edit");
  });

  it("should return false", function () {
    let result = createPatientRow(true);
    expect(result).toBeFalsy();
  });

  it("should return false", function () {
    let result = createPatientRow(null);
    expect(result).toBeFalsy();
  });

  it("should return false", function () {
    let patient = {
      id: 1,
      nombre: "Name",
      lastName: "LastName",
      symptoms: "Sintoma 1",
    };
    let result = createPatientRow(patient);
    expect(result).toBeFalsy();
  });

  it("should return false", function () {
    let patient = [1, 2, 3, 4];
    let result = createPatientRow(patient);
    expect(result).toBeFalsy();
  });

  it("checks for the use of the function replaceChild", function () {
    spyOn(document, "getElementById").and.callFake(function (id) {
      if (id === "patientsData") {
        return $patientsTable;
      } else {
        return document.getElementById(id);
      }
    });
    replaceData($fragment);
    expect(document.getElementById).toHaveBeenCalled();
  });
});
