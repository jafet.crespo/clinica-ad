
import { getAll } from "../src/Pages/dashboard/dashboardCRUD.js";


describe("Fetch data test", function () {
  beforeEach(function () {
    
    jasmine.Ajax.install();
  });

  afterEach(function () {
    jasmine.Ajax.uninstall();
  });

  it("checks if the getAll function returns the correct data", function (done) {
    jasmine.Ajax.stubRequest(`http://localhost:3000/pacientes`).andReturn({
      status: 200,
      contentType: "application/json",
      responseText: '{"id": 1, "title": "Test title", "body": "Test body"}',
    });

    getAll().then(function (data) {
      expect(data).toEqual({
        id: 1,
        title: "Test title",
        body: "Test body",
      });
      done();
    });
  });
});
