import { validarUsuario } from "../src/Pages/login/validarLogin.js";

describe("validarUsuario function test", function () {
  beforeEach(function () {});

  it("should return true with username 'admin' and password 'admin'", function () {
    let validar = validarUsuario("admin", "admin");
    expect(validar).toBeTruthy();
  });

  it("should return false with username = 'asd' and password = 'asd'", function () {
    let validar = validarUsuario("asd", "asd");
    expect(validar).toBeFalsy();
  });

  it("should return false with username = 1 and password = 2 ", function () {
    let validar = validarUsuario(1, 2);
    expect(validar).toBeFalsy();
  });

  it("should return false with username = 'admin' and password = undefined ", function () {
    let validar = validarUsuario("admin");
    expect(validar).toBeFalsy();
  });
});