import ordenarBurbuja from "../src/helpers/ordenarBurbuja.js";

let data;

describe("ordenarBurbuja function test", function () {
  beforeEach(function () {
    data = [
      { id: 1, name: "x name" },
      { id: 2, name: "a name" },
      { id: 3, name: "d name" },
    ];
  });

  it("should return sortedData", function () {
    let sortedData = [
      { id: 2, name: "a name" },
      { id: 3, name: "d name" },
      { id: 1, name: "x name" },
    ];
    let result = ordenarBurbuja(data, "name");
    expect(result).toEqual(sortedData);
  });

  it("should return false", function () {
    let result = ordenarBurbuja([1, 2, 3, 4], "name");
    expect(result).toBeFalsy();
  });

  it("should return false", function () {
    let result = ordenarBurbuja(data, "apellido");
    expect(result).toBeFalsy();
  });

  it("should return false", function () {
    let result = ordenarBurbuja(null, "apellido");
    expect(result).toBeFalsy();
  });
});
